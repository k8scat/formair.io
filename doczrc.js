export default {
    title: 'ForMaiR',
    description: 'auto Forward eMails with custom Rules',
    ignore: ['README.md'],
    files: 'src/**/*.{md,markdown,mdx}'
}